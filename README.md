# Run project
1. install docker
2. run: `docker-compose up -d `
3. wait for start (to check logs `docker-compose logs` )
4. app available on 3000 port
5. hooks don't support localhost, to test it locally you can use something like https://github.com/localtunnel/localtunnel

# Run tests
To run tests: 
- you should have docker up
- `npm i`
- `npm run tests`

# Note
For simplicity project uses in memory db, it means after each restart data will be clear
