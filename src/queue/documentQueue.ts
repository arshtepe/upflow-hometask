import Queue, { Job } from "bee-queue";
import { Container } from 'typedi';
import config from "../config";
import { DocumentProcessService } from "../services/DocumentProcessService";

type JobResult = {
    documentId: string
}

type JobData = {
    fileURL: string
    clientId: string
}

export const documentQueue = new Queue('document', { redis: config.redis.url() });

documentQueue.process(async function (job: Job<JobData>) {
    const documentProcessService = Container.get(DocumentProcessService)
    return documentProcessService.process(job.data.fileURL)
});

documentQueue.on('succeeded', ({ data }: Job<JobData>, result: JobResult) => {
    const documentProcessService = Container.get(DocumentProcessService)
    return documentProcessService.processed(data.clientId, result.documentId)
});

documentQueue.on('failed', ({ data }: Job<JobData>, err: Error) => {
    const documentProcessService = Container.get(DocumentProcessService)
    return documentProcessService.failed(data.clientId, err)
});