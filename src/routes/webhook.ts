import express from "express"
import logger from "../config/logger";
import { WebHook } from "../domain/WebHook";
import { checkNotEmptyValue } from "../utils/utils";

const router = express.Router()

router.post("/webhook", (req, res) => {
    const clientId = checkNotEmptyValue("clientId can't be empty", 400, req.body.clientId);
    const url = checkNotEmptyValue("url can't be empty", 400, req.body.url);
    const webHook = new WebHook(clientId, url)

    webHook.save()
        .then(() => res.sendStatus(201))
        .catch(err => {
            logger.error(`can't register webhook ${err}`)
            res.status(500).send("Can't register webhook")
        })
})


export default router