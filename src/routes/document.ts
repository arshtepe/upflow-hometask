import express from "express"
import logger from "../config/logger";
import { documentQueue } from "../queue/documentQueue";
import { checkNotEmptyValue } from "../utils/utils";

const router = express.Router()

router.post('/document', function (req, res) {
    documentQueue.createJob({
        fileURL: checkNotEmptyValue("fileURL can't be empty", 400, req.body.fileURL),
        clientId: checkNotEmptyValue("clientId can't be empty", 400, req.body.clientId),
    })
        .save()
        .then(() => {
            res.sendStatus(201)
        }).catch(err => {
        logger.error(`Can't schedule document ${err}`)
        res.status(500).send("Can't upload document")
    })
})

export default router