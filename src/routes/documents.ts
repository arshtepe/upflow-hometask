import express from "express"
import config from "../config"
import { Document } from "../domain/Document";
import { checkNotEmptyParams, checkThat } from "../utils/utils";

const router = express.Router()

const MAX_DOCUMENTS_LIMIT = 100;

router.get("/documents", (req, res) => {
    const limit = parseInt(checkNotEmptyParams("limit can't be empty", req.query?.limit as string));
    const offset = parseInt(checkNotEmptyParams("offset can't be empty", req.query?.offset as string));

    checkThat(limit < MAX_DOCUMENTS_LIMIT, 422, `limit more than max limit ${MAX_DOCUMENTS_LIMIT}`);

    Document.find({
        skip: offset,
        take: limit,
    }).then(docs => {
        const result = docs.map(doc => ({
            id: doc.id,
            document: `/${config.filesFolder}/${doc.hashSum}/${doc.hashSum}.pdf`,
            thumbnail: `/${config.filesFolder}/${doc.hashSum}/${Document.thumbnailName()}`
        }))

        res.json(result)
    })
})

export default router