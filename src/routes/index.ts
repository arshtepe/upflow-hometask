import express from "express"
import document from "./document"
import documents from "./documents"
import webhook from "./webhook"

export function registerRoutes(app: express.Application) {
    app.use(webhook)
    app.use(document)
    app.use(documents)
}