import 'reflect-metadata';
import express from "express"
import fs from "fs/promises"
import bodyParser from "body-parser"
import config from "./config/index"
import logger from "./config/logger"
import { createDBConnection } from "./config/db"
import { registerRoutes } from "./routes";
import { removeFolder } from "./utils/utils";

(async function () {
    process.on('uncaughtException', function(err: Error) {
        // prevent app crashing
        logger.error('uncaughtException', err)
    })

    await cleanup()
    const app = express()

    app.use('/static', express.static(config.filesPath))
    app.use(bodyParser.json())

    await createDBConnection()
    registerRoutes(app)

    app.listen(config.port, () => {
        console.log(`App listening at port:${config.port}`)
    }).on('error', err => {
        logger.error(err);
        process.exit(1);
    });
}())



async function cleanup() {
    await removeFolder(config.tmpDir)
    return fs.mkdir(config.tmpDir)
}

