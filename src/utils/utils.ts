import fsPromise from "fs/promises";
import createError from "http-errors"

export function removeFolder(pathToFolder: string): Promise<void> {
    return fsPromise.rmdir(pathToFolder, { recursive: true })
}

export function checkThat(condition: boolean, status: number, message: string) {
    if (!condition) {
        throw createError(status, message)
    }
}

export function checkNotEmptyValue<T>(message: string, statusCode: number, val?: T): T {
    if (val === undefined || val === null) {
        throw createError(statusCode, message)
    }
    return val
}

export function checkNotEmptyParams<T>(message: string, val?: T): T {
    return checkNotEmptyValue(message, 400, val)
}
