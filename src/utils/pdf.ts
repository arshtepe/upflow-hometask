import Canvas, { Canvas as CanvasType } from 'canvas'
import fs from 'fs-extra'
import { PDFDocumentProxy } from "pdfjs-dist/types/display/api";

const pdfLib = require('pdfjs-dist/legacy/build/pdf.js');

type ThumbnailOptions = {
    width: number
    height: number
    pageNumber: number
}

export async function generateThumbnail(pathToPdf: string, dest: string, options: ThumbnailOptions = {
    width: 100,
    height: 100,
    pageNumber: 1
}) {
    const doc = await pdfLib.getDocument(pathToPdf).promise;
    const countOfPages = doc.numPages;
    if (countOfPages >= options.pageNumber) {
        const preview = await renderPreview(doc, options)
        await fs.writeFile(dest, preview);
    }
}

async function renderPreview(doc: PDFDocumentProxy, options: ThumbnailOptions): Promise<Buffer> {
    const page = await doc.getPage(options.pageNumber);
    const viewport = page.getViewport({ scale: 1.0 });
    const realSizeCanvas = Canvas.createCanvas(viewport.width, viewport.height);
    const context = realSizeCanvas.getContext("2d");
    const renderTask = page.render({ canvasContext: context, viewport: viewport });
    await renderTask.promise
    return resizeCanvas(realSizeCanvas, options.width, options.height)
}

function resizeCanvas(original: CanvasType, width: number, height: number): Buffer {
    const resultCanvas = Canvas.createCanvas(width, height);
    resultCanvas
        .getContext("2d")
        .drawImage(original, 0, 0, width, height)
    return resultCanvas.toBuffer()
}