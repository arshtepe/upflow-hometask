import { Document } from "./Document"
import { WebHook } from "./WebHook"

export default [Document, WebHook]