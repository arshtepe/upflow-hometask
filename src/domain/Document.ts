import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Document extends BaseEntity {

    public static thumbnailName() {
        return "thumbnail.png"
    }

    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    hashSum: string;

    constructor(hashSum: string) {
        super();
        this.hashSum = hashSum;
    }

}