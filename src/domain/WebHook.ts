import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class WebHook extends BaseEntity {

    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    clientId: string;

    @Column()
    url: string;

    constructor(
        clientId: string,
        url: string,
    ) {
        super();

        this.clientId = clientId;
        this.url = url;
    }
}