import dotenv from 'dotenv';
import path from 'path';
import { path as rootPath } from 'app-root-path';

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const envFound = dotenv.config();
if (envFound.error) {
    throw new Error("⚠️  Couldn't find .env file  ⚠️");
}

export default {
    port: parseInt(notEmpty("PORT", process.env.PORT), 10),

    logs: {
        level: process.env.LOG_LEVEL || 'silly',
    },
    db: {
        database: notEmpty("DATABASE", process.env.DATABASE),
    },
    redis: {
        port: parseInt(notEmpty("REDIS_PORT", process.env.REDIS_PORT)),
        host: notEmpty("REDIS_HOST", process.env.REDIS_HOST),
        url() {
            return `${this.host}:${this.port}/`
        }
    },

    filesFolder: "static",
    filesPath: path.join(rootPath, "static"),
    tmpDir: path.join(rootPath, "tmp"),
    storagePath: path.join(rootPath, "static")
};

function notEmpty<T>(name: string, value?: T): T {
    if (value === undefined || value === null) {
        throw new Error(`value of ${name} can't be enpty`)
    }
    return value
}