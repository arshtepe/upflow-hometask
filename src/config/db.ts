import { createConnection } from "typeorm";
import entities from "../domain"
import config from "./index"

export function createDBConnection() {
    return createConnection({
        type: "sqlite",
        database: config.db.database,
        dropSchema: true,
        synchronize: true,
        logging: false,
        entities: entities
    });
}
