import fetch from "node-fetch";
import pRetry from "p-retry";
import { Service } from 'typedi';
import { WebHook } from "../domain/WebHook";

export enum EventType {
    DocumentCreated = "DocumentCreated",
    Failed = "Failed"
}

export type WebHookEvent = {
    type: EventType
    documentId?: string
    error?: string
}
const RETRY_COUNT = 1;

@Service()
export class WebhookService {
    notifySubscribers(clientId: string, event: WebHookEvent) {
        return pRetry(async () => {
            const webhooks = await WebHook.find({ where: { clientId } })
            const requests = webhooks.map(webhook => fetch(webhook.url, {
                method: 'POST',
                body: JSON.stringify(event),
                headers: { 'Content-Type': 'application/json' }
            }));
            await Promise.all(requests)
        }, { retries: RETRY_COUNT })
    }
}
