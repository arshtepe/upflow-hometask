import { Service } from "typedi";
import { DocumentService } from "./DocumentService";
import { EventType, WebhookService } from "./WebhookService";
import logger from "../config/logger";

@Service()
export class DocumentProcessService {

    private documentService: DocumentService
    private webhookService: WebhookService

    constructor(documentService: DocumentService, webhookService: WebhookService) {
        this.documentService = documentService;
        this.webhookService = webhookService;
    }

    public async process(fileURL: string) {
        const document = await this.documentService.processDocument(fileURL)
        return { documentId: document.id }
    }

    public processed(clientId: string, documentId: string) {
        return this.webhookService.notifySubscribers(clientId, {
            type: EventType.DocumentCreated,
            documentId: documentId
        }).catch(err => {
            logger.error(`Can't send webhook to client ${clientId} because of ${err}`)
        })
    }

    public failed(clientId: string, err: Error) {
        logger.info(`Document for client ${clientId} wasn't processed because of ${err}`)
        this.webhookService.notifySubscribers(clientId, {
            type: EventType.Failed,
            error: err.message
        }).catch(err => {
            logger.error(`Can't send webhook to client ${clientId} because of ${err}`)
        })
    }

}