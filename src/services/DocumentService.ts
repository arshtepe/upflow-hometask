import { createWriteStream, move, pathExists } from "fs-extra";
import crypto from "crypto";
import fetch from "node-fetch";
import config from "../config"
import path from "path";
import { Service } from 'typedi';
import { v4 as uuidv4 } from "uuid";
import limitStream from 'size-limit-stream'
import { generateThumbnail } from "../utils/pdf";
import { Document } from "../domain/Document";

const FIVE_MB_SIZE_LIMIT = 1024 * 1024 * 10

@Service()
export class DocumentService {

    async processDocument(fileURL: string) {
        const id = uuidv4();
        const tmpFileName = `${id}.pdf`;
        const tmpFilePath = path.join(config.tmpDir, tmpFileName);
        const fileHash = await this.loadDocument(fileURL, tmpFilePath, FIVE_MB_SIZE_LIMIT)
        const existedDocument = await Document.findOne({ where: { hashSum: fileHash } });

        if (existedDocument) {
            return existedDocument
        } else {
            return this.createDocument(tmpFilePath, fileHash)
        }
    }

    private async createDocument(tmpFilePath: string, fileHash: string) {
        const destinationFolder = path.join(config.storagePath, fileHash)
        const documentPath = path.join(destinationFolder, `${fileHash}.pdf`);
        const isPathExists = await pathExists(documentPath)
        if (!isPathExists) {
            await move(tmpFilePath, documentPath)
        }
        const doc = new Document(fileHash)
        await doc.save();
        await generateThumbnail(documentPath, path.join(destinationFolder, Document.thumbnailName()));
        return doc
    }

    private async loadDocument(url: string, dest: string, maxFileSize: number): Promise<string> {
        const file = createWriteStream(dest);
        const hashSum = crypto.createHash('sha256');
        const response = await fetch(url);

        if (!response.ok) throw new Error(`Can't load file ${response.statusText}`);

        const limitedStream = response.body.pipe(limitStream(maxFileSize))

        limitedStream.pipe(file);

        response.body.on("data", chunk => hashSum.update(chunk))

        return new Promise((resolve, reject) => {
            file.on("finish", () => resolve(hashSum.digest('hex')));
            const errorHandler = (err: Error) => reject(err);
            file.on("error", errorHandler);
            limitedStream.on("error", errorHandler);
        });
    }

}
