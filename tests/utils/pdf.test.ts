import path from "path";
import fs from "fs-extra";
import { generateThumbnail } from "../../src/utils/pdf";

jest.mock("fs-extra")

const TEST_FILE = path.join(__dirname, "test.pdf")

afterEach(() => jest.clearAllMocks())

describe("generateThumbnail", () => {

    it('should save preview', async () => {
        //when
        await generateThumbnail(TEST_FILE, "dest")

        // then
        expect(fs.writeFile).toBeCalled()
    });

    it('should skip if page more than document size', async() => {
        //when
        await generateThumbnail(TEST_FILE, "dest", {
            width: 100,
            height: 100,
            pageNumber: 5
        })

        // then
        expect(fs.writeFile).not.toBeCalled()
    });
})



