import { HttpError } from "http-errors";
import { checkNotEmptyParams, checkNotEmptyValue, checkThat } from "../../src/utils/utils";

it('checkThat should throw error if condition failed', () => {
    expect(() => {
        checkThat(false, 400, "")
    }).toThrow(HttpError);
});


describe("checkNotEmptyValue", () => {
    it('should throw error if value empty',  () => {
        expect(() => {
            checkNotEmptyValue(  "error", 400, undefined)
        }).toThrow(HttpError);
    });

    it('should return value if value not empty', () => {
        // given
        const val = "value"

        //when
        const result = checkNotEmptyValue(  "error", 400, val)

        expect(result).toEqual(val)
    });
})

describe("checkNotEmptyParams", () => {
    it('should throw error if value empty', () => {
        expect(() => {
            checkNotEmptyParams(  "error", undefined)
        }).toThrow(HttpError);
    });

    it('should return value if value not empty', () => {
        // given
        const val = "value"

        //when
        const result = checkNotEmptyParams(  "error", val)

        expect(result).toEqual(val)
    });
})

