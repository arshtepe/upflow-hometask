import { Document } from "../../src/domain/Document";
import { DocumentProcessService } from "../../src/services/DocumentProcessService";
import { mocked } from "ts-jest/utils";
import { DocumentService } from "../../src/services/DocumentService";
import { EventType, WebhookService } from "../../src/services/WebhookService";

jest.mock("../../src/services/DocumentService")
jest.mock("../../src/services/WebhookService")

jest.mock('node-fetch');

afterEach(() => jest.clearAllMocks())

describe('Document Process Service', () => {
    it('should process document', async () => {
        // given
        const url = "file/url"
        const documentService = mocked(new DocumentService)
        const webhookService = mocked(new WebhookService)
        const documentProcessService = new DocumentProcessService(documentService, webhookService)
        const document = new Document("hash");

        documentService.processDocument.mockResolvedValue(document)

        // when
        const result = await documentProcessService.process(url)

        // then
        expect(result.documentId).toEqual(document.id)
        expect(documentService.processDocument).toBeCalled()
    });

    it('should send webhook when succeed', async () => {
        // given
        const documentService = mocked(new DocumentService)
        const webhookService = mocked(new WebhookService)
        const documentProcessService = new DocumentProcessService(documentService, webhookService)
        const clientId = Math.random().toString()
        const documentId = Math.random().toString()

        webhookService.notifySubscribers.mockReturnValue(Promise.resolve())

        // when
        await documentProcessService.processed(clientId, documentId)

        // then
        expect(webhookService.notifySubscribers).toBeCalledWith(clientId, {
            type: EventType.DocumentCreated,
            documentId: documentId
        })
    });

    it('should send webhook when failed', async () => {
        // given
        const documentService = mocked(new DocumentService)
        const webhookService = mocked(new WebhookService)
        const documentProcessService = new DocumentProcessService(documentService, webhookService)
        const clientId = Math.random().toString()
        const error = new Error("error")

        webhookService.notifySubscribers.mockReturnValue(Promise.resolve())

        // when
        await documentProcessService.failed(clientId, error)

        // then
        expect(webhookService.notifySubscribers).toBeCalledWith(clientId, {
            type: EventType.Failed, error: error.message
        })
    });


});
