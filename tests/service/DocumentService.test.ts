import fetch from 'node-fetch';
import { createWriteStream, pathExists } from "fs-extra"
import { mocked } from "ts-jest/utils"
import inMemoryStreams from "memory-streams"
import crypto from "crypto";
import path from "path";
import { createConnection, getConnection, getRepository } from "typeorm";
import { WriteStream } from "fs";
import { DocumentService } from "../../src/services/DocumentService";
import { Document } from "../../src/domain/Document";
import config from "../../src/config";
import { generateThumbnail } from "../../src/utils/pdf";


const { Response }  = jest.requireActual("node-fetch")

jest.mock('node-fetch');
jest.mock('fs-extra');
jest.mock('../../src/utils/pdf.ts', () => ({
    generateThumbnail: jest.fn()
}));

beforeAll(() => {
    return createConnection({
        type: "sqlite",
        database: ":memory:",
        dropSchema: true,
        entities: [Document],
        synchronize: true,
        logging: false
    });
});

afterAll(() => getConnection().close())
afterEach(() => {
    jest.clearAllMocks()
})


describe('Document Service', () => {
    it('should create new document if doesn\'t exist', async () => {
        // given
        const url = "file/url"
        const documentService = new DocumentService()
        const hashSum = crypto.createHash('sha256');
        const fileData = "data"
        hashSum.update(fileData)
        const fileHash = hashSum.digest('hex')
        const destFolder = path.join(config.storagePath, fileHash)
        const destFilePath = path.join(destFolder, `${fileHash}.pdf`)
        const pathExistsMock = <jest.Mock<Promise<boolean>>>pathExists;
        const writableStream = new inMemoryStreams.WritableStream() as any;  // library has broken typing
        const createWriteStreamMock = createWriteStream as jest.Mock<WriteStream>;

        mocked(fetch).mockResolvedValue(new Response(readStream(fileData)))
        createWriteStreamMock.mockReturnValue(writableStream)
        pathExistsMock.mockResolvedValue(true)

        // when
        await documentService.processDocument(url)

        // then
        const document = await getRepository(Document).find({ hashSum: fileHash })

        expect(document).not.toBeNull()
        expect(generateThumbnail).toBeCalledWith(destFilePath, path.join(destFolder, Document.thumbnailName()))
        expect(pathExistsMock).toBeCalledWith(destFilePath)
        expect(createWriteStreamMock).toBeCalled()
        expect(fetch).toBeCalledWith(url)
    });

    it('should return existing document if duplicate', async () => {
        // given
        const url = "file/url"
        const documentService = new DocumentService()
        const hashSum = crypto.createHash('sha256');
        const fileData = Math.random().toString()
        hashSum.update(fileData)
        const fileHash = hashSum.digest('hex')
        const document = await new Document(fileHash).save()
        const writableStream = new inMemoryStreams.WritableStream() as any;  // library has broken typing
        const createWriteStreamMock = createWriteStream as jest.Mock<WriteStream>;

        mocked(fetch).mockResolvedValue(new Response(readStream(fileData)))
        createWriteStreamMock.mockReturnValue(writableStream)

        // when
        const res = await documentService.processDocument(url)

        // then
        expect(res.id).toEqual(document.id)
        expect(generateThumbnail).not.toBeCalled()
        expect(createWriteStreamMock).toBeCalled()
        expect(fetch).toBeCalledWith(url)
    });

});


function readStream(fileData: string) {
    const stream = new inMemoryStreams.ReadableStream({ emitClose: true } as any); // library has broken typing
    stream.push(fileData)
    stream.push(null)
    return stream
}