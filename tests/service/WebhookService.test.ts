import fetch from 'node-fetch';
import { createConnection, getConnection, getRepository } from "typeorm";
import { EventType, WebhookService } from "../../src/services/WebhookService";
import { WebHook } from "../../src/domain/WebHook";

jest.mock('node-fetch', () => jest.fn());

beforeAll(() => {
    return createConnection({
        type: "sqlite",
        database: ":memory:",
        dropSchema: true,
        entities: [WebHook],
        synchronize: true,
        logging: false
    });
});

afterAll(() => getConnection().close())
afterEach(() => jest.clearAllMocks())

describe('Webhook Service', () => {
    it('should sent event to subscriber', async () => {
        // given
        const clientId = Math.random().toString()
        const url = "/hello/hook"
        await getRepository(WebHook).insert({
            url,
            clientId: clientId
        });
        const webhookService = new WebhookService()
        const event = { type: EventType.DocumentCreated, documentId: Math.random().toString() };

        // when
        await webhookService.notifySubscribers(clientId, event)

        // then
        expect(fetch).toBeCalledWith(url, {
                "body": JSON.stringify(event),
                "headers": { "Content-Type": "application/json" },
                "method": "POST"
            }
        )
    });

    it('should skip if no events for client', async () => {
        // given
        await getRepository(WebHook).insert({
            url: "/hello/hook",
            clientId: Math.random().toString()
        });
        const clientId = Math.random().toString()
        const webhookService = new WebhookService()
        const event = { type: EventType.DocumentCreated, documentId: Math.random().toString() };

        // when
        await webhookService.notifySubscribers(clientId, event)

        // then
        expect(fetch).not.toBeCalled()
    });

});